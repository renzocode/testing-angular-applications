import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TestingComponentsComponent} from './testing-components/testing-components.component';

const routes: Routes = [
  {
    path: 'vista/:id',
    component: TestingComponentsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
