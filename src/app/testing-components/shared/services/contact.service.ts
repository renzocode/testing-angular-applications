import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Contact } from '../';
import { map, switchMap, catchError, find } from 'rxjs/operators';
import { empty } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  private contactsUrl = 'http://jsonplaceholder.typicode.com/todos/1';
  private headers = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient) { }

  public getContacts(): Observable<any> {
    return this.http.get<Contact>(this.contactsUrl)
      .pipe(
        map(contact => {
          return contact;
        }),
        catchError(this.handleError));
  }

  public getContact(id: number): Observable<Contact> {
    return this.getContacts()
      .pipe(
        map(contact => {
          return contact;
        }))
      .pipe(
        find((contact: any) => {
          if (contact.id === id) {
            return true;
          } else {
            return false;
          }
        }));
  }

  public save(contact: Contact): Observable<Contact> {
    if (contact.id) {
      return this.put(contact);
    }

    return this.post(contact);
  }

  public new(contact: Contact): Observable<Contact> {
    return this.post(contact);
  }

  public delete(contact: Contact): Observable<Contact> {
    const url = `${this.contactsUrl}/${contact.id}`;

    return this.http
             .delete(url, {headers: this.headers})
      .pipe(switchMap(() => empty()), catchError(this.handleError));
    }

  public post(contact: Contact): Observable<Contact> {
    return this.http
        .post(this.contactsUrl, JSON.stringify(contact), {headers: this.headers})
      .pipe(catchError(this.handleError));
  }

  public put(contact: Contact): Observable<Contact> {
    const url = `${this.contactsUrl}/${contact.id}`;

    return this.http
      .put(url, JSON.stringify(contact), {headers: this.headers}).pipe(
          map(() => contact),
          catchError(this.handleError)
      );
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
