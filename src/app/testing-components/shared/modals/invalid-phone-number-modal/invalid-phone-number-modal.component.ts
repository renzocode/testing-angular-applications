import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-invalid-phone-number-modal',
  templateUrl: './invalid-phone-number-modal.component.html',
  styleUrls: ['./invalid-phone-number-modal.component.scss']
})
export class InvalidPhoneNumberModalComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
