import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-invalid-email-modal',
  templateUrl: './invalid-email-modal.component.html',
  styleUrls: ['./invalid-email-modal.component.scss']
})
export class InvalidEmailModalComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
