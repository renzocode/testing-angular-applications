import { Directive, ElementRef, Input,
  HostListener, OnInit, Renderer2 } from '@angular/core';
import { constants } from './favorite-icon.constants';

@Directive({
  selector: '[appFavoriteIcon]'
})
export class FavoriteIconDirective implements OnInit {
  private element: HTMLElement;
  private renderer: Renderer2;
  private primaryColor = 'gold';
  private startClasses: any = constants.classes;

  @Input('appFavoriteIcon') isFavorite: boolean;
  @Input() set color(primaryColorName: string) {
    if (primaryColorName) {
      this.primaryColor = primaryColorName.toLowerCase();
      this.setSolidColoredStar();
    }
  }

  constructor(
    element: ElementRef, renderer: Renderer2
  ) {
    this.element = element.nativeElement;
    this.renderer = renderer;
   }

  ngOnInit(): void {
    if (this.isFavorite) {
      this.setSolidColoredStar();
    } else {
      this.setWhiteSolidStar();
    }
  }

  @HostListener('mouseenter')
  public onMouseEnter(): void {
    if (!this.isFavorite) {
      this.setBlackOutlineStar();
    }
  }

  @HostListener('mouseleave')
  public onMouseLeave(): void {
    if (!this.isFavorite) {
      this.setWhiteSolidStar();
    }
  }

  @HostListener('click')
  public onClick(): void {
    this.isFavorite = !this.isFavorite;
    if (this.isFavorite) {
      this.setSolidColoredStar();
    } else {
      this.setBlackOutlineStar();
    }
  }

  private setBlackOutlineStar(): void {
    this.setStarColor('black');
    this.setStarClass('outline');
  }

  private setSolidColoredStar(): void {
    this.setStarColor(this.primaryColor);
    this.setStarClass('solid');
  }

  private setWhiteSolidStar(): void {
    this.setStarColor('white');
    this.setStarClass('solid');
  }

  private setStarClass(starType: string): void {
    const className: string = this.getStartClasses(starType);
    this.renderer.setAttribute(this.element, 'class', className);
  }

  private setStarColor(color: string): void {
    this.renderer.setStyle(this.element, 'color', color);
  }

  private getStartClasses(starType): string {
    let classNames = '';

    switch (starType) {
      case 'solid':
        classNames = this.startClasses.SOLID_STAR;
        break;
      case 'outline':
        classNames = this.startClasses.OUTLINE_STAR;
        break;
      default:
        classNames = this.startClasses.SOLID_STAR;
    }
    return classNames;
  }
}
