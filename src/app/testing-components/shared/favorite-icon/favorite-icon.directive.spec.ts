import { Component } from '@angular/core';
import { ComponentFixture, TestBed, TestModuleMetadata } from '@angular/core/testing';
import { constants } from './favorite-icon.constants';
import { FavoriteIconDirective } from './favorite-icon.directive';
import { getStarElement } from '../../testing/get-star-element';
import { doClassesMatch } from '../../testing/do-classes-match';

@Component({
  template: `
  <i [appFavoriteIcon]="true"></i>
  <i [appFavoriteIcon]="false"></i>
  <i [appFavoriteIcon]="true" [color]="'blue'"></i>
  <i [appFavoriteIcon]="true" [color]="'cat'"></i>
` 
})
class TestComponent { }
describe('Directive: FavoriteIconDirective', () => {
  let fixture: ComponentFixture<any>;
  const expectedSolidStartList = constants.classes.SOLID_STAR_STYLE_LIST;
  const expectedOutlineStarList  = constants.classes.OUTLINE_STAR_STYLE_LIST;

  beforeEach(() => {
    const testModuleMetadata: TestModuleMetadata = {
      declarations: [FavoriteIconDirective, TestComponent ]
    };
    fixture = TestBed.configureTestingModule(testModuleMetadata)
      .createComponent(TestComponent);
    fixture.detectChanges();
  });

  describe('when favorite icon is set to true', () => {
    let starElement = null;
    beforeEach(() => {
      const defaultTrueElementIndex = 0;
      starElement = getStarElement(fixture, defaultTrueElementIndex);
      console.log(starElement);
    });

    it('Should display a solid gold star after the page loads', () => {
      expect(starElement.style.color).toBe('gold');
      expect(doClassesMatch(starElement.classList, expectedSolidStartList)).toBeTruthy();
    });

    it('Should display a black outline of a star the user rolls over the start', () => {
      const event = new Event('click');
      starElement.dispatchEvent(event);
      expect(starElement.style.color).toBe('black');
      expect(doClassesMatch(starElement.classList, expectedOutlineStarList)).toBeTruthy();
    });
    it('Should display a solid gold star if the user rolls over the start', () => {
      const event = new Event('mouseenter');
      starElement.dispatchEvent(event);
      expect(starElement.style.color).toBe('gold');
      expect(doClassesMatch(starElement.classList, expectedSolidStartList)).toBeTruthy();
    });
  });
});
