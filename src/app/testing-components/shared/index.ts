export * from './data/mock-contacts';
export * from './models/contact.model';
export * from './favorite-icon/favorite-icon.directive';
export * from './modals/invalid-email-modal/invalid-email-modal.component';
export * from './modals/invalid-phone-number-modal/invalid-phone-number-modal.component';
export * from './services/contact.service';
export * from './phone-number/phone-number.pipe';

