import { Pipe, PipeTransform } from '@angular/core';
import { PhoneNumber } from './phone-number.model';
import { phoneNumberErrorMessages } from './phone-number-error-messages';

@Pipe({
  name: 'phoneNumber'
})
export class PhoneNumberPipe implements PipeTransform {

  transform(value: string = '', format?: string, countryCode: string = '', allowEmptyString?: boolean): string {
    let phoneNumber: PhoneNumber = null;
    let formattedPhoneNumber = '';

    if (allowEmptyString && value === '') {
      return '';
    }

    if (this.isPhoneNumberValid(value)) {
      phoneNumber = new PhoneNumber(value);
      formattedPhoneNumber = phoneNumber.getFormattedPhoneNumber(format, countryCode);
    }
    return formattedPhoneNumber;
  }

  private isPhoneNumberValid(phoneNumber: any): boolean {
    const VALID_PHONE_LENGTH = 10;
    let isPhoneNumberValid = false;

    if (isNaN(phoneNumber)) {
      console.error(phoneNumberErrorMessages.INVALID_PHONE_NUMBER_TYPE_ERR);
    } else if (phoneNumber.toString().length !== VALID_PHONE_LENGTH) {
      console.error(phoneNumberErrorMessages.INVALID_PHONE_NUMBER_LENGTH_ERR);
    } else {
      isPhoneNumberValid = true;
    }
    return isPhoneNumberValid;
  }
}
