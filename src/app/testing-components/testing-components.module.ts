import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TestingComponentsComponent } from './testing-components.component';
import { ContactsEditComponent } from './contacts-edit/contacts-edit.component';
import { FavoriteIconDirective } from './shared/favorite-icon/favorite-icon.directive';
import { InvalidEmailModalComponent } from './shared/modals/invalid-email-modal/invalid-email-modal.component';
import { InvalidPhoneNumberModalComponent } from './shared/modals/invalid-phone-number-modal/invalid-phone-number-modal.component';
import { HttpClientModule } from '@angular/common/http';
import { ContactService } from './shared/services';
import { AppMaterialModule } from './../app.material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ContactListComponent } from './contact-list/contact-list.component';
import { PhoneNumberPipe } from './shared/phone-number/phone-number.pipe';

@NgModule({
  declarations: [
    TestingComponentsComponent,
    // Component testing
    ContactsEditComponent,
    InvalidPhoneNumberModalComponent,
    InvalidEmailModalComponent,
    // Directive testing
    ContactListComponent,
    FavoriteIconDirective,
    PhoneNumberPipe,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    AppMaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    ContactService
  ]
})
export class TestingComponentsModule {
  constructor() {
    console.log('testing components');
  }
}
