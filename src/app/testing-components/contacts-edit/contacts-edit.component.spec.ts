import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { DebugElement, InjectionToken } from '@angular/core';
import { ContactsEditComponent } from './contacts-edit.component';
import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { Contact, ContactService, FavoriteIconDirective, InvalidEmailModalComponent, InvalidPhoneNumberModalComponent } from '../shared';
import { AppMaterialModule} from 'src/app/app.material.module';
import { HttpClientModule } from '@angular/common/http';
import { MatSnackBarModule } from '@angular/material/snack-bar';
// Shadow tests
describe('ContactsEditComponent test', () => {
  let fixture: ComponentFixture<ContactsEditComponent>;
  let component: ContactsEditComponent;
  let rootElement: DebugElement;

  const contactServiceStub = {
    contact: {
      id: 1,
      name: 'Lorace',
      favorite: false
    },

    async save(contact: Contact): Promise<void> {
      component.contact = contact;
    },

    async getContact(): Promise<any> {
      component.contact = this.contact;
      return this.contact;
    },

    async updateContact(contact: Contact): Promise<void> {
      component.contact = contact;
    }
  };

  // Configure TestBed
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        ContactsEditComponent,
        FavoriteIconDirective,
        InvalidEmailModalComponent,
        InvalidPhoneNumberModalComponent
      ],
      imports : [
        AppMaterialModule,
        FormsModule,
        NoopAnimationsModule,
        RouterTestingModule,
        HttpClientModule,
        MatSnackBarModule,
      ],
      providers: [
        ContactService,
        {
          provide: InjectionToken, useValue: contactServiceStub, multi: true
        }
      ]
    });

    TestBed.overrideModule(BrowserDynamicTestingModule, {
      set: {
        entryComponents: [
          InvalidEmailModalComponent,
          InvalidPhoneNumberModalComponent
        ]
      }
    });
  });
  // Create Component by TestBed
  beforeEach(() => {
    fixture = TestBed.createComponent(ContactsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    rootElement = fixture.debugElement;
  });

  describe('saveContact() test', () => {
    it('should display contact name after contact set', fakeAsync(() => {
      const contact = {
        id: 1,
        name: 'lorace'
      };
      component.isLoading = false;
      component.saveContact(contact);
      fixture.detectChanges();
      const nameInput = rootElement.query(By.css('.contact-name'));
      tick();
      expect(nameInput.nativeElement.value).toBe('lorace');
    }));
  });
  describe('loadContact() test', () => {
    it('should load contact', fakeAsync(() => {
      component.isLoading = false;
      // spyOn(ContactService,'getContacta').and.returnValue(of([{id: 1}]).pipe(delay(1));
      // component.loadContact();
      fixture.detectChanges();
      tick(1500);
    }));
  });
  describe('updateContact() test', () => {
    it('should update the contact', fakeAsync(() => {
      const newContact = {
        id: 1,
        name: 'delia',
        email: 'delia@example.com',
        number: '1234567890'
      };
      component.contact = {
        id: 1,
        name: 'rhonda',
        email: 'rhonda@example.com',
        number: '1234567890'
      };
      component.isLoading = false;
      fixture.detectChanges();
      const nameInput = rootElement.query(By.css('.contact-name'));
      tick();
      expect(nameInput.nativeElement.value).toBe('rhonda');
      component.updateContact(newContact);
      fixture.detectChanges();
      tick(100);
      expect(nameInput.nativeElement.value).toBe('delia');
    }));
  });
  /*
   it('Should create', () => {
    expect(component).toBeDefined();
  });

  it('should contain "Loading contact..."', () => {
    const bannerElement: HTMLElement = fixture.nativeElement;
    expect(bannerElement.textContent).toContain('Loading contact...');
  });

  it('should have <h6> with "Loading contact..."', () => {
    const bannerElement: HTMLElement = fixture.nativeElement;
    const h6 = bannerElement.querySelector('h6');
    expect(h6.textContent).toEqual('Loading contact...');
  });

  it('should find the <h6> with fixture.debugElement.nativeElement', () => {
    const bannerDe: DebugElement = fixture.debugElement;
    const bannerEl: HTMLElement = bannerDe.nativeElement;
    const h6 = bannerEl.querySelector('h6');
    expect(h6.textContent).toEqual('Loading contact...');
  });

  it('should find <h6> with fixture.debugElement.query(By.css)', () => {
    const bannerDe: DebugElement = fixture.debugElement;
    const titleDe = bannerDe.query(By.css('h6'));
    const h6: HTMLElement = titleDe.nativeElement;
    expect(h6.textContent).toEqual('Loading contact...');
  });
  */
});
