import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement, InjectionToken } from '@angular/core';
import { ContactListComponent } from './contact-list.component';
import { Contact, ContactService, FavoriteIconDirective, InvalidEmailModalComponent, InvalidPhoneNumberModalComponent } from '../shared';
import { AppMaterialModule } from 'src/app/app.material.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';

describe('ContactListComponent', () => {
  let fixture: ComponentFixture<ContactListComponent>;
  let component: ContactListComponent;
  let rootElement: DebugElement;

  const contactServiceStub = {
    contact: {
      id: 1,
      name: 'Lorace',
      favorite: false
    },

    async save(contact: Contact): Promise<void> {

    },

    async getContact(): Promise<any> {
      return this.contact;
    },

    async updateContact(contact: Contact): Promise<void> {
    }
  };


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ContactListComponent,
        FavoriteIconDirective,
      ],
      imports: [
        AppMaterialModule,
        NoopAnimationsModule,
        RouterTestingModule,
        HttpClientModule,
        MatSnackBarModule,
      ],
      providers: [
        ContactService,
        {
          provide: InjectionToken, useValue: contactServiceStub, multi: true
        }
      ]
    })
    .compileComponents();

    TestBed.overrideModule(BrowserDynamicTestingModule, {
      set: {
        entryComponents: [
          InvalidEmailModalComponent,
          InvalidPhoneNumberModalComponent
        ]
      }
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
