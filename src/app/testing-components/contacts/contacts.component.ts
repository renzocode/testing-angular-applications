import { Component, OnInit } from '@angular/core';

export interface IContact {
  id: number;
  name: string;
}


@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent implements OnInit {

  public contacts = [];
  constructor() { }

  ngOnInit(): void {
  }

}
