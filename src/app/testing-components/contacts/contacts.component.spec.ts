/*
import {
  ComponentFixture,
  TestBed } from '@angular/core/testing';

import { ContactsComponent, IContact } from './contacts.component';

// Isolated tests
describe('ContactsComponent', () => {

  let contactsComponent: ContactsComponent = null;

  beforeEach(() => {
    contactsComponent = new ContactsComponent();
  });

  it('Should set intance correctly', () => {
    expect(contactsComponent).not.toBeNull();
  });

  it('Should be no contacts if there is no data', () => {
    expect(contactsComponent.contacts.length).toBe(0);
  });

  it('Should be contacts if there is data', () => {
    const newContact: IContact = {
      id: 1,
      name: 'Jason Pipemaker'
    };
    const contactsList: Array<IContact> = [newContact];
    contactsComponent.contacts = contactsList;

    expect(contactsComponent.contacts.length).toBe(1);
  });
});

// Shadow tests
*/
